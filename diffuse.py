"""
Copyright 2013 Jason M. Hite

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""



import numpy as np
import scipy.sparse.linalg as la
import scipy.sparse as spa
from spdotter import pMatrixDotter
import pylab
from gmres import gmres

def buildOperator(mat, gp):
    nv = 3 * mat.ncells + 2
    op = np.zeros((nv, nv))
    for i in xrange(mat.ncells):
        p_i = i
        j_i = mat.ncells + 1 + i
        pb_i = 2 * mat.ncells + 2 + i

        #eq 1
        op[i, j_i] = -1.
        op[i, j_i + 1] = 1.
        op[i, pb_i] = mat['sigr', gp][i] * mat.dx 

        #eq 2
        op[mat.ncells + i, p_i + 1] = mat['d', gp][i]
        op[mat.ncells + i, pb_i] = -1 * mat['d', gp][i]
        op[mat.ncells + i, j_i + 1] = .5 * mat.dx

        #eq 3
        op[i + 2 * mat.ncells, pb_i] = mat['d', gp][i]
        op[i + 2 * mat.ncells, p_i] = -1 * mat['d', gp][i]
        op[i + 2 * mat.ncells, j_i] = .5 * mat.dx

    #BCs
    op[-2, 2 * mat.ncells + 2] = 1.
    op[-2, 0] = -1.
    op[-1, mat.ncells] = 1.
    op[-1, -1] = -1.

    #Not converting to sparse -> will be done later
    return op

def Q(mat, phib, phib_new, k, gp):
    if gp == 1:
        qs = mat['sigs', 2, 1] * phib[1]
        qf = mat['nuf', 1] * mat['sigf', 1] * phib[0] + \
             mat['nuf', 2] * mat['sigf', 2] * phib[1]

    if gp == 2:
        qs = mat['sigs', 1, 2] * phib_new[0]
        qf = mat['nuf', 1] * mat['sigf', 1] * phib[0] + \
             mat['nuf', 2] * mat['sigf', 2] * phib[1]
    
    qf *= mat['chi', gp] / k

    return mat.dx * (qs + qf)

def dk(mat, state, g):
    phib = state[g - 1, 2 * mat.ncells + 2:]
    j = state[g - 1, mat.ncells + 1: 2*mat.ncells + 2]

    s1 = (mat['nuf', g] * mat['sigf', g] * phib).sum()
    s2 = j[-1] - j[0]
    s3 = (mat['siga', g] * phib).sum()

    return s1, s2 + s3

epsk = 1e-2
epsp = 1e-2

#This is a coroutine that tracks iterations and convergence
def ctester(phi, k, imax):
    i = 0
    phi_old = phi
    k_old = k
    status = False
    while status is False:
        new_data = (yield status)
        if new_data is not None:
            i += 1
            phi_new, k_new = new_data
            if np.abs(1 - k_old / k_new) < epsk and \
              np.abs(1 - phi_old.max() / phi_new.max()) < epsp:
                status = True
            phi_old = phi_new
            k_old = k_new
            if i >= imax:
                raise Exception("Maximum iterations exceeded \
                                 before convergence.")
    print i, "iterations required for convergence"
    yield status


def diffuse(mat, dtol=1e-2):
    #dtol controls the tolerance for the ilu factors
    #1e-2 is about right for the 2 assembly problem

    nv = 3 * mat.ncells + 2
    R = map(lambda g: buildOperator(mat, g), [1, 2])
    print "Operator dimensions: ", R[0].shape
    #Build preconditioners for each group
    M = map(lambda x: la.spilu(spa.csc_matrix(x), drop_tol=dtol), R)
    #Build parallel matrix vector multipliers
    R = map(lambda x: pMatrixDotter(x), R)

    #state = np.random.rand(2, nv)
    state = np.random.rand() * np.ones((2, nv))
    k = np.random.rand()

    phib = state[:, 2 * mat.ncells + 2:]

    ctest = ctester(phib, k, 15)
    ctest.next() #Initialize the coroutine
                 #for the convergence test
    converged = False
    
    while not converged:
        new_state = np.zeros_like(state)
        phib = state[:, 2 * mat.ncells + 2:]
        phib_new = new_state[:, 2 * mat.ncells + 2:].view()
        
        q0 = np.hstack((Q(mat, phib, phib_new, k, 1),
                        np.zeros(2 * mat.ncells + 2)))
        new_state[0, :] = gmres(R[0], q0, state[0, :], 50, M=M[0])
        
        q1 = np.hstack((Q(mat, phib, phib_new, k, 2),
                        np.zeros(2 * mat.ncells + 2)))
        new_state[1, :] = gmres(R[1], q1, state[1, :], 50, M=M[1])

        new_state /= phib_new.sum()

        a1, b1 = dk(mat, new_state, 1)
        a2, b2 = dk(mat, new_state, 2)

        k_new = (a1 + a2) / (b1 + b2)

        converged = ctest.send((phib_new, k_new))

        state = new_state
        k = k_new

    return k, state
