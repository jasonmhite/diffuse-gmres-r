"""
Copyright 2013 Jason M. Hite

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""



from comp import material
from data import *
import operator
import diffuse
import numpy as np
import pylab

cell1 = material(wat, 2, 20) + \
        material(mox, 10, 100) + material(wat, 2, 20)
cell2 = material(wat, 2, 20) + \
        material(ura, 10, 100) + material(wat, 2, 20)

problem = reduce(operator.add, 8 * [cell1] + 8 * [cell2])

print "Number of cells: ", problem.ncells
k, state = diffuse.diffuse(problem)
print "k= ", k
phib = state[:, 2 * problem.ncells + 2:]
#If you want to see currents or edge fluxes, figure
#it out yourself.

pylab.figure()
pylab.subplot(211)
pylab.title('Group 1 cell-center flux')
pylab.plot(phib[0, :])
pylab.subplot(212)
pylab.title('Group 2 cell-center flux')
pylab.plot(phib[1, :])
pylab.show()
