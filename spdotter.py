"""
Copyright 2013 Jason M. Hite

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""



from IPython.parallel import Client
import uuid

c = Client()
dview = c[:]
view = c.load_balanced_view()

with dview.sync_imports():
    import numpy 
    import scipy.sparse
    import scipy.sparse.linalg

class pMatrixDotter(object):
    def __init__(self, A, sparse=True):
        self.view = dview
        #Unique id to make sure variable names don't clash if there
        #are several pMatrixDotters, gets appended to variable names.
        #Not the most elegant, but effective
        self.uid = uuid.uuid4().hex
        self.shape = A.shape

        #Send the dense version because it's simpler
        #Only happens once, so the cost isn't that bad
        self.view.scatter('a%s' % self.uid, A)

        if sparse:
            #Sparsify the slices on the remote nodes
            self.view.execute('a%s=scipy.sparse.csr_matrix(a%s)' \
                    % (self.uid, self.uid))

    def matvec(self, x):
        self.view['x%s' % self.uid] = x #Implies broadcast to all slaves
        self.view.execute('x%s=a%s.dot(x%s)' % tuple(3 * [self.uid]))

        res = self.view.gather('x%s' % self.uid) #Concatenate results

        return res.get()

    def __del__(self):
        #Variables on the slave nodes have to be explicitly cleaned
        #up in the destructor
        self.view.execute('del x%s' % self.uid)
        self.view.execute('del a%s' % self.uid)
        
        self.super.__del__() #This was omitted in the original version, 
                             #I think it needs to be here. Untested :).
