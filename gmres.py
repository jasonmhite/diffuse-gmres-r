"""
Copyright 2013 Jason M. Hite

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""



import numpy as np
import scipy.sparse as sp
import scipy.sparse.linalg as sla
from spdotter import pMatrixDotter

"""
This is an implementation of restarted GMRES based on the MATLAB version
given by Netlib at http://www.netlib.org/templates/matlab/gmres.m , which
is available in the public domain. 

This implements GMRES(r) with an incremental QR factorization using 
Householder rotations to solve the least squares problem in the GMRES 
algorithm. It optionally uses a preconditioner, which should provide a
.solve() method, similar to what is provided by scipy.sparse.linalg.spilu.
In fact, you will have good luck if you use spilu and will probably have
bad luck using no preconditioner.

A should *not* be a NumPy array. It should be something that implements a
.matvec() method, e.g. a scipy.sparse.linalg.LinearOperator or the 
pMatrixDotter class that should be included with this code. It should also 
have a .shape attribute like a NumPy array.

Some tweaks have been made to make it faster in Python.
"""

def rotmat(a, b):
    if b == 0.:
        c = 1.
        s = 0.
    elif np.abs(b) > np.abs(a):
        t = a / b
        s = 1. / np.sqrt(1 + t ** 2)
        c = t * s
    else:
        t = b / a
        c = 1. / np.sqrt(1 + t ** 2)
        s = t * c

    return c, s

def gmres(A, b, x0, restart, M=False, imax=20, \
          eps=.0001, quiet=True, debug=False):
    #Ah assumed to be a pMatrixDotter or
    #a LinearOperator (=> has .matvec)
    x = x0

    beta = np.linalg.norm(b)

    if M:
        r = M.solve(b - A.matvec(x))
    else:
        r = b - A.matvec(x)
    n = A.shape[0]

    m = restart - 1 #To make up for Numpy array indexing
                    #weirdness.
    V = np.zeros((n, m + 1))
    H = np.zeros((m + 1, m))

    c = np.zeros(m)
    sn = np.zeros(m)
    e = np.zeros(n)
    e[0] = 1

    ti = 0
    
    for it in xrange(imax):
        if M:
            r = M.solve(b - A.matvec(x))
        else:
            r = b - A.matvec(x)

        V[:, 0] = r / np.linalg.norm(r)

        s = np.linalg.norm(r) * e
        for i in xrange(m):
            ti += 1
            if M:
                w = M.solve(A.matvec(V[:, i]))
            else:
                w = A.matvec(V[:, i])
            for k in xrange(i + 1): 
                H[k, i] = np.dot(w, V[:, k])
                w -= H[k, i] * V[:, k]
            H[i + 1, i] = np.linalg.norm(w)
            V[:, i + 1] = w / H[i + 1, i]

            for k in xrange(i):
                t = c[k] * H[k, i] + sn[k] * H[k + 1, i]
                H[k + 1, i] = -sn[k] * H[k, i] + c[k] * H[k + 1, i]
                H[k, i] = t

            c[i], sn[i] = rotmat(H[i, i], H[i + 1, i])
            t = c[i] * s[i]
            s[i + 1] = -sn[i] * s[i]
            s[i] = t
            H[i, i] = c[i] * H[i, i] + sn[i] * H[i + 1, i]
            H[i + 1, i] = 0.
            err = np.abs(s[i + 1]) / beta

            if err <= eps:
                y = np.linalg.solve(H[:i + 1, :i + 1], s[:i + 1])
                x += np.dot(V[:, :i + 1], y)
                break

        if err <= eps: break
        y = np.linalg.solve(H[:m, :m], s[:m])
        x += np.dot(V[:, :m], y)
        if M:
            r = M.solve(b - A.matvec(x))
        else:
            r = b - A.matvec(x)
        s[i + 1] = np.linalg.norm(r)
        err = s[i + 1] / beta
        if err <= eps: break
    
    if debug:
        print ti, err

    if quiet:
        return x
    else:
        return x, err, ti
