"""
Copyright 2013 Jason M. Hite

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""



__all__ = ['mox', 'ura', 'wat']

mox = dict()
ura = dict()
wat = dict()

mox[('sigt', 1)] = .2
ura[('sigt', 1)] = .2
wat[('sigt', 1)] = .2
mox[('sigs', 1, 1)] = .185
ura[('sigs', 1, 1)] = .185
wat[('sigs', 1, 1)] = .17
mox[('sigs', 1, 2)] = .015
ura[('sigs', 1, 2)] = .015
wat[('sigs', 1, 2)] = .03
mox[('sigf', 1)] = 0.
ura[('sigf', 1)] = 0.
wat[('sigf', 1)] = 0.
mox[('nuf', 1)] = 0.
ura[('nuf', 1)] = 0.
wat[('nuf', 1)] = 0.
mox[('chi', 1)] = 1.
ura[('chi', 1)] = 1.
wat[('chi', 1)] = 0.
mox[('mub', 1)] = 0.
ura[('mub', 1)] = 0.
wat[('mub', 1)] = 0.
mox['d', 1] = 1. / (3 * mox['sigt', 1])
ura['d', 1] = 1. / (3 * ura['sigt', 1])
wat['d', 1] = 1. / (3 * wat['sigt', 1])
mox['sigr', 1] = mox['sigt', 1] - mox['sigs', 1, 1] 
ura['sigr', 1] = ura['sigt', 1] - ura['sigs', 1, 1]
wat['sigr', 1] = wat['sigt', 1] - wat['sigs', 1, 1]
mox['siga', 1] = mox['sigr', 1] - mox['sigs', 1, 2]
ura['siga', 1] = ura['sigr', 1] - ura['sigs', 1, 2]
wat['siga', 1] = wat['sigr', 1] - wat['sigs', 1, 2]

mox[(('sigt', 2))] = 1.2
ura[(('sigt', 2))] = 1.
wat[(('sigt', 2))] = 1.1
mox[('sigs', 2, 2)] = .9
ura[('sigs', 2, 2)] = .9
wat[('sigs', 2, 2)] = 1.1
mox[('sigs', 2, 1)] = 0.
ura[('sigs', 2, 1)] = 0.
wat[('sigs', 2, 1)] = 0.
mox[('sigf', 2)] = .3
ura[('sigf', 2)] = .1
wat[('sigf', 2)] = 0.
mox[('nuf', 2)] = 1.9
ura[('nuf', 2)] = 1.7
wat[('nuf', 2)] = 0.
mox[('chi', 2)] = 0.
ura[('chi', 2)] = 0.
wat[('chi', 2)] = 0.
mox[('mub', 2)] = 0.
ura[('mub', 2)] = 0.
wat[('mub', 2)] = 0.
mox['d', 2] = 1. / (3 * mox['sigt', 2])
ura['d', 2] = 1. / (3 * ura['sigt', 2])
wat['d', 2] = 1. / (3 * wat['sigt', 2])
mox['sigr', 2] = mox['sigt', 2] - mox['sigs', 2, 2]
ura['sigr', 2] = ura['sigt', 2] - ura['sigs', 2, 2]
wat['sigr', 2] = wat['sigt', 2] - wat['sigs', 2, 2]
mox['siga', 2] = mox['sigr', 2] - mox['sigs', 2, 1]
ura['siga', 2] = ura['sigr', 2] - ura['sigs', 2, 1]
wat['siga', 2] = wat['sigr', 2] - wat['sigs', 2, 1]

