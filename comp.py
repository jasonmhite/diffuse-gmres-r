"""
Copyright 2013 Jason M. Hite

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""



from __future__ import division
import numpy as np
from data import *
from copy import deepcopy

class material(object):
    def __init__(self, properties, L, ncells):
        self.ncells = ncells
        self.L = L
        self.dx = L / ncells
        self.p = properties

        self._buildmesh()

    def _buildmesh(self):
        self.mesh = dict()
        for key in self.p.keys():
            self.mesh[key] = np.empty(self.ncells)
            self.mesh[key][:] = self.p[key]

    def __add__(self, other): 
        #Deep copy to build new composite material from the inputs
        new = deepcopy(self)
        for key in new.p.keys():
            new.mesh[key] = np.hstack((new.mesh[key], 
                                       other.mesh[key]))
        new.L += other.L
        new.ncells += other.ncells
        new.dx = new.L / new.ncells
        return new

    def __getitem__(self, item):
        return self.mesh[item]

